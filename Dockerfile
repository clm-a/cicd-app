FROM hitalos/laravel
COPY . /var/www/
RUN composer install
CMD php ./artisan serve --port=$PORT --host=0.0.0.0