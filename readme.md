# How to CI CD

## 0. Pré-requis

Avoir un vrai shell Ubuntu sous Windows, une machine virtuelle avec Ubuntu et, dans tous les cas, Docker CE installé et à jour

## 1. Créer une application laravel

Avec laravel qui fonctionne déjà sur votre machine :

`laravel new mon_app`

Ou bien en utilisant une image Docker adéquate (en cherchant un peu parmi les dizaines publiées).  
L'image est plutôt à jour et simple, basée sur Linux Alpine ( = légère )

``docker run --rm -v `pwd`:/var/www hitalos/laravel composer create-project --prefer-dist laravel/laravel mon_app``

_Les fichiers ont été créés en root par Docker sur la machine hôte, il faut donc peut-être changer le propriétaire des fichiers :_

``sudo chown -R $USER:$USER mon_app``

## 2. Prendre connaissance de la page [Docker hub](https://hub.docker.com/r/hitalos/laravel/dockerfile) et du [Dockerfile](https://hub.docker.com/r/hitalos/laravel/dockerfile) de cette image

## 3. Démarrer l'application à l'aide de l'image pour voir si tout va bien


`cd mon_app`

``docker run --rm -ti -v `pwd`:/var/www -p 80:80 hitalos/laravel``

_Ici, on monte le dossier courant dans /var/www car c'est le workdir défini dans l'image docker_


## 4. Packager l'application complète avec son code source

Créer un fichier `Dockerfile` :

```dockerfile
FROM hitalos/laravel
COPY . /var/www/
RUN composer install       # installer les dépendances du projet, si besoin
CMD php ./artisan serve --port=$PORT --host=0.0.0.0 # on surcharge la commande lancée avec le container,
                                                    # pour choisir un port arbitraire (c.f. deploy)
```

_On note que la commande de base de l'image n'est pas modifiée par défaut, lancer un container va donc lancer le server Php_

Construire l'image pour tester tout ça :

`docker build -t mon-app .`

Lancer un container à partir de l'image

`docker run --rm -ti -p 80:80 -e PORT=80 mon-app`

_Note1: on peut supprimer le container juste après son exécution avec --rm  
Note2 : on spécifie une variable d'environnement PORT qui nous permet de spécifier un port arbitraire (c.f. étape deploy)_

On doit voir l'application sur localhost

Next : essayer de passer les tests en surchargeant la commande par défaut de l'image (lancer le serveur) avec la commande phpunit

`docker run --rm mon-app phpunit`

On doit constater que les tests passent.

## 5. Préparer l'intégration continue

_La partie piégeuse est que chaque étape récrée un environnement vierge, aussi devons-nous passer l'image Docker générée d'une étape à l'autre via un [artifact](https://forum.gitlab.com/t/passing-docker-image-between-build-and-test-stage-in-gitlab-runner/2444/6)_

Créer un fichier `.gitlab-ci.yml`


```yml
image: docker:latest

services:
  - docker:dind

stages:
  - build
  - test

docker_package:
  stage: build
  script:
    - docker build -t mon-app .
    - mkdir images
    - docker save mon-app > images/mon-app.tar   # ici, on exporte l'image pour s'en reservir dans l'étape build
  artifacts:
    paths:
      - images                                   # ici, on préserve le dossier "images" créé juste au-dessus pour le passer dans l'étape build

run_tests:
  stage: test
  script:
    - docker load -i images/mon-app.tar                # ici on recharge l'image générée et passée de l'étape précédente
    - docker run --rm -e APP_KEY=$APP_KEY mon-app phpunit   # Avec Laravel, la variable d'environnement APP_KEY doit être définie,
                                                            # on la passe ici après l'avoir créée dans Settings > CI / CD > Environment variables
                                                            # grâce à php artisan key:generate --show
```

_Remarque, comme on Dockerize tout on utilise l'image Docker de base et le service dind (Docker in Docker)_


## 6. Envoyer le tout sur le Gitlab

`git push URL_DU_DEPOT_GITLAB`

Dans l'onglet "CI / CD" une pipeline docker_package > run_tests devrait être définie, et le processus devrait être lancé.
